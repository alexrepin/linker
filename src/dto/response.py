from typing import List
from pydantic.main import BaseModel
from src.enum.status import StatusResponse


class Response(BaseModel):
    status: StatusResponse


class Domains(BaseModel):
    domains: List[str]
    status = StatusResponse.OK
