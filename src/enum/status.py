from enum import Enum


class StatusResponse(Enum):
    OK = 'ok'
    PING = 'ping'
    EMPTY_LINKS = 'empty links'
