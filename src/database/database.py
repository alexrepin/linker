import os
import psycopg2


class Database:
    connection: psycopg2.extensions.connection = None
    cursor: psycopg2.extensions.cursor = None

    def __init__(self) -> None:
        self.host = os.environ.get('DB_HOST')
        self.port = os.environ.get('DB_PORT')
        self.user = os.environ.get('DB_USER')
        self.password = os.environ.get('DB_PASS')
        self.base = os.environ.get('DB_NAME')

    def connect(self) -> None:
        try:
            self.connection = psycopg2.connect(
                user=self.user,
                password=self.password,
                host=self.host,
                port=self.port,
                database=self.base,
                connect_timeout=10,
            )

            self.connection.autocommit = True
            self.cursor = self.connection.cursor()
            self.init()
        except (Exception, psycopg2.Error) as e:
            raise Exception(f'Connection error: {str(e)}')

    def init(self) -> None:
        try:
            self.cursor.execute(open('resources/schema.sql', 'r').read())
        except (Exception, psycopg2.Error) as e:
            raise Exception(f'Failed create main table: {str(e)}')

    def close(self) -> None:
        try:
            self.cursor.close()
            self.connection.close()
        except (Exception, psycopg2.Error):
            pass
