from fastapi import FastAPI
from src.controller import ping, visited_links, visited_domains
from src.database import db

app = FastAPI()


@app.on_event('startup')
async def on_startup():
    app.state.db = db
    app.state.db.connect()

    app.include_router(ping)
    app.include_router(visited_links)
    app.include_router(visited_domains)


@app.on_event('shutdown')
async def on_shutdown() -> None:
    app.state.db.close()
