import datetime
from src.database import Database
from src.dto.response import Domains


class GetDomains:
    @staticmethod
    def do(db: Database, from_time: datetime.datetime, to_time: datetime.datetime) -> Domains:
        db.cursor.execute(
            'select distinct domain from visited_links where created_at >= %s and created_at <= %s',
            (from_time.strftime('%Y-%m-%d %H:%M:%S'), to_time.strftime('%Y-%m-%d %H:%M:%S'))
        )

        return Domains(domains=[domain[0] for domain in db.cursor.fetchall()])
