import re
import datetime
from typing import List
from src.database import Database


class SaveLinks:
    @staticmethod
    def do(db: Database, links: List[str]) -> None:
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        placeholders = ','.join(['(%s, %s, %s)' for _ in range(len(links))])
        items = [(link, SaveLinks.extract_domain(link), now) for link in links]

        db.cursor.executemany('insert into visited_links (link, domain, created_at) values ' + placeholders, items)

    @staticmethod
    def extract_domain(link: str) -> str:
        return re.search(r"\w+\.\w+", link).group()
