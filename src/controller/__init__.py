from src.controller.ping import router as ping
from src.controller.visited_links import router as visited_links
from src.controller.visited_domains import router as visited_domains
