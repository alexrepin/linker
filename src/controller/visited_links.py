from typing import List
from pydantic.main import BaseModel
from pydantic.networks import HttpUrl
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from fastapi import Request
from starlette.exceptions import HTTPException
from src.action.save_links import SaveLinks
from src.dto.response import Response
from src.enum.status import StatusResponse

router = InferringRouter()


class SaveVisitedLinksRequest(BaseModel):
    links: List[HttpUrl]


@cbv(router)
class VisitedLinksController:
    @router.post('/visited_links')
    async def save(self, origin: Request, request: SaveVisitedLinksRequest) -> Response:
        if len(request.links) == 0:
            raise HTTPException(status_code=422, detail=StatusResponse.EMPTY_LINKS.value)

        SaveLinks.do(origin.app.state.db, request.links)

        return Response(status=StatusResponse.OK)
