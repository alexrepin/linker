from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from src.dto.response import Response
from src.enum.status import StatusResponse

router = InferringRouter()


@cbv(router)
class PingController:
    @router.get('/ping')
    async def ping(self) -> Response:
        return Response(status=StatusResponse.PING)
