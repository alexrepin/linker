import datetime
from typing import Optional
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from fastapi import Request
from src.action.get_domains import GetDomains
from src.dto.response import Domains

router = InferringRouter()


@cbv(router)
class VisitedDomainsController:
    @router.get('/visited_domains')
    async def get(
            self,
            origin: Request,
            from_time: Optional[int] = None,
            to_time: Optional[int] = None
    ) -> Domains:
        if from_time is None:
            from_time = datetime.datetime.now() - datetime.timedelta(days=30)
        else:
            from_time = datetime.datetime.fromtimestamp(from_time)

        if to_time is None:
            to_time = datetime.datetime.now()
        else:
            to_time = datetime.datetime.fromtimestamp(to_time)

        return GetDomains.do(
            origin.app.state.db,
            from_time,
            to_time
        )
