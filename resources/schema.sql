create table if not exists public.visited_links
(
    id bigserial primary key,
    link varchar(255) not null,
    domain varchar(255) not null,
    created_at timestamp(0)
);

create index if not exists created_at_idx on visited_links(created_at);
create index if not exists domain_idx on visited_links(domain);