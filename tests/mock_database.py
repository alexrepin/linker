from src.database import Database


class MockCursor:
    def execute(self, *args) -> None:
        pass

    def executemany(self, *args) -> None:
        pass

    def fetchall(self) -> []:
        return []


class MockDatabase(Database):
    cursor: MockCursor = MockCursor()

    def connect(self) -> None:
        pass

    def close(self) -> None:
        pass
