from src.action.save_links import SaveLinks
from tests.mock_database import MockDatabase


def test_save_links() -> None:
    error = False

    try:
        SaveLinks.do(
            MockDatabase(),
            ['https://ya.ru']
        )
    except:
        error = True

    assert error is False


def test_extract_domain() -> None:
    assert SaveLinks.extract_domain('https://ya.ru') == 'ya.ru'
