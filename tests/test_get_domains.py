import datetime
from src.action.get_domains import GetDomains
from src.dto.response import Domains
from tests.mock_database import MockDatabase


def test_get_domains() -> None:
    from_time = datetime.datetime.now() - datetime.timedelta(days=30)
    to_time = datetime.datetime.now()
    domains = GetDomains.do(
        MockDatabase(),
        from_time,
        to_time
    )

    assert domains == Domains(domains=[])
