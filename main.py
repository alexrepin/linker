import os
import uvicorn
from dotenv import load_dotenv

load_dotenv(override=False)

if __name__ == '__main__':
    uvicorn.run(
        app='src.app:app',
        host='0.0.0.0',
        port=int(os.environ.get('PORT', 3000)),
        reload=int(os.environ.get('DEBUG', 0)) > 0,
        workers=1
    )
