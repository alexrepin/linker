fastapi==0.110.0
fastapi_utils==0.2.1
python-dotenv==1.0.1
uvicorn==0.28.0
psycopg2-binary==2.9.9
types-psycopg2==2.9.21.20240311
pytest==8.1.1